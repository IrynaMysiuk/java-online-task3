package epam.zoo;

import java.util.ArrayList;
import java.util.List;

public class Zoo {

    private List<Animal> animals;

    public Zoo() {
        animals = new ArrayList<Animal>();
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public int getAnimalCount() {
        return animals.size();
    }

    public String getAnimal(String name) {
        String animalProperty = null;
        for (Animal animal : animals)
            if (name.equals(animal.getName())) {
                animalProperty = "Name: " + animal.getName() +
                        "\tAge: " + animal.getAge() +
                        "\tCountry: " + animal.getContinent() +
                        "\tGender: " + animal.getGender();
                break;
            } else
                animalProperty = "The animal \"" + name + "\" is missing in Zoo";
        return animalProperty;
    }

    public String getAnimals() {
        StringBuilder allAnimals = new StringBuilder();
        allAnimals.append("-----Animal list:-----\n");
        for (Animal animal : animals) {
            allAnimals.append(" Name: " + animal.getName() +
                    ", Age: " + animal.getAge() +
                    ", Country: " + animal.getContinent() +
                    ", Gender: " + animal.getGender() + "\n");
        }

        return allAnimals.toString();
    }
}
