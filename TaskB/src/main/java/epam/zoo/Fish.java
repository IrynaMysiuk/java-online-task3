package epam.zoo;

public abstract class Fish extends Animal {
    public abstract void swim();
}
