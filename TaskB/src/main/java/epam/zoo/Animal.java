package epam.zoo;

public abstract class Animal {
    private String name;
    private int age;
    private String gender;
    private String continent;

    public abstract Animal sleep();

    public abstract Animal eatAnything(Food food);

    public abstract Animal makeSound();

    public Animal setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Animal setAge(int age) {
        this.age = age;
        return this;
    }

    public String getGender() {
        return gender;
    }

    public Animal setGender(Gender gender) {
        this.gender = gender.toString();
        return this;
    }

    public String getContinent() {
        return continent;
    }

    public Animal setContinent(Continent continent) {
        this.continent = continent.toString();
        return this;
    }
}
