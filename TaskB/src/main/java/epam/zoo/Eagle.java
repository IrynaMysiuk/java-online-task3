package epam.zoo;

public class Eagle extends Bird {

    @Override
    public Animal fly() {
        System.out.println("I can fly!");
        return this;
    }

    @Override
    public Animal sleep() {
        System.out.println("I prefer sleep at the night!");
        return this;
    }

    @Override
    public Animal eatAnything(Food food) {
        System.out.println("I like eat " + food);
        return this;
    }

    @Override
    public Animal makeSound() {
        System.out.println("Kruuuu!");
        return this;
    }

}
