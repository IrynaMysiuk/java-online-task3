package epam.zoo;

public class Parrot extends Bird {

    @Override
    public Animal sleep() {
        System.out.println("I prefer sleep at the night!");
        return this;
    }

    @Override
    public Animal eatAnything(Food food) {
        System.out.println("I like eat " + food);
        return this;
    }

    @Override
    public Animal makeSound() {
        System.out.println("I can say: \"I'm parrot\"");
        return this;
    }

    @Override
    public Animal fly() {
        System.out.println("I can fly!");
        return this;
    }
}
