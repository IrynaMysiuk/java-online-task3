package epam.zoo;

public enum Continent {
    EUROPE("Europe"),
    AMERICA("America"),
    ASIA("Asia"),
    AFRICA("Africa");
    private String continent;

    Continent(String continent) {
        this.continent = continent;
    }

    public String toString() {
        return continent;
    }
}
