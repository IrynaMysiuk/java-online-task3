package epam.zoo;

public enum Gender {
    MAN("man"),
    WOMAN("women");
    private String gender;

    Gender(String gender) {
        this.gender = gender;
    }

    public String toString() {
        return gender;
    }
}
