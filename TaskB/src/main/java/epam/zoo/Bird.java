package epam.zoo;

public abstract class Bird extends Animal {
    public abstract Animal fly();
}
