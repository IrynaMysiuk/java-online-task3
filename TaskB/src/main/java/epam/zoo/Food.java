package epam.zoo;

public enum Food {

    PLANTS("plants"),
    MEALS("meals"),
    FRUITS("fruit"),
    ANYTHING("anything");
    private String food;

    Food(String food) {
        this.food = food;
    }

    public String toString() {
        return food;
    }

}
