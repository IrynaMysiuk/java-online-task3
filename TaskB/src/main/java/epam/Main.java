package epam;

import epam.shapes.Circle;
import epam.shapes.Rectangle;
import epam.shapes.Shapes;
import epam.shapes.Triangle;
import epam.zoo.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter task number: \n1-zoo, \n2-shape: ");
        int choice = scanner.nextInt();

        switch (choice) {
            case 1: {
                Zoo zoo = new Zoo();
                zoo.addAnimal(new Eagle()
                        .setName("John")
                        .setAge(1)
                        .setContinent(Continent.AMERICA)
                        .setGender(Gender.WOMAN)
                        .eatAnything(Food.MEALS)
                        .sleep()
                        .makeSound());
                zoo.addAnimal(new Parrot()
                        .setName("Ula")
                        .setAge(2)
                        .setContinent(Continent.AFRICA)
                        .setGender(Gender.MAN)
                        .eatAnything(Food.PLANTS)
                        .sleep()
                        .makeSound());

                System.out.println("There are " + zoo.getAnimalCount() + " animals in Zoo!");

                System.out.println(zoo.getAnimal("John"));
                System.out.println(zoo.getAnimal("Bobik"));
                System.out.println("Enter animal name: ");
                String animalName = scanner.nextLine();
                System.out.println(zoo.getAnimal(animalName));

                System.out.println(zoo.getAnimals());
                break;
            }
            case 2: {
                List<Shapes> shapeList = new ArrayList<Shapes>();
                shapeList.add(new Rectangle(2.0, 4.0, "red"));
                shapeList.add(new Triangle(3.0, "blue"));
                shapeList.add(new Circle(5.0, "green"));

                for (Shapes shape : shapeList)
                    System.out.println(shape.toString());
                break;
            }
            default:
                System.out.println("Incorrect value!");
                break;
        }
    }
}
