package epam.shapes;

public class Rectangle extends Shapes {
    private double width;
    private double height;

    public Rectangle(double width, double height, String color) {
        super("Rectangle", color);
        this.width = width;
        this.height = height;
    }

    public double getPerimeter() {
        return 2.0 * (width + height);
    }

    public double getArea() {
        return width * height;
    }
}
