package epam.shapes;

public class Triangle extends Shapes {
    private double side;

    public Triangle(double side, String color) {
        super("Triangle", color);
        this.side = side;
    }

    public double getPerimeter() {
        return 3 * side;
    }

    public double getArea() {
        return (Math.sqrt(3) / 4) * side * side;
    }
}
