package epam.shapes;

public abstract class Shapes {
    private String color;
    private String name;

    Shapes(String name, String color) {
        this.color = color;
        this.name = name;
    }

    protected String getColor() {
        return color;
    }

    protected void setColor(String color) {
        this.color = color;
    }

    protected String getName() {
        return name;
    }

    public abstract double getArea();

    public abstract double getPerimeter();

    @Override
    public String toString() {
        return String.format("Shape name: %s, color: %s, area: %s, perimeter: %s",
                getName(), getColor(), getArea(), getPerimeter());
    }
}
